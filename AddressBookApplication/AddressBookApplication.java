/*
 * Program:AddressBookApplication
 * By: Andy Ho
 * Date: 1/12/2016
 * Update: 1/13/2016 - User can now fill in 2 addresses and it will display on console
 * Purpose: User fills out address cards and it will display on console
 * 
 */

public class AddressBookApplication {

	static AddressBook ab;
	
	static public void initAddressBookExercise(){
		AddressCard firstEntry = new AddressCard(); //Creates first AddressCard object
		AddressCard secondEntry = new AddressCard(); // and the second. Both prompts to fill out ob

		//Step1 you need to save firstEntry  and secondEntry in ab.addressBookList 
		
		//Step2 you cycle through the ab.addressBookList and print out through using the toString on AddressEntry
		System.out.println(firstEntry.toString()); // print to console all the info
		System.out.println(secondEntry.toString());
	}
	
	public static void main(String[] args){ //Calls the exercise method
		initAddressBookExercise();
	}
	
}